#include <inttypes.h>

struct pix {
    uint8_t r, g, b;
};

struct image {
    struct pix* data;
    uint64_t width, height;
};
