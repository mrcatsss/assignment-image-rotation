#include "bmp_.h"
#include "internal.h"

#ifndef IMAGE_TRANSFORMER_BMP_CREATE_H
#define IMAGE_TRANSFORMER_BMP_CREATE_H
struct bmp_header create_header(const struct image* image);
#endif //IMAGE_TRANSFORMER_BMP_CREATE_H
