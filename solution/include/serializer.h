#include <stdio.h>

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_ERROR,
    READ_SEEK_ERROR
};

enum read_status from_bmp(FILE* input, struct image* img);

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_PADD_ERROR
};

enum write_status to_bmp(FILE* output, struct image const* img);

