#ifndef OPENER_H
#define OPENER_H

#include  <stdint.h>
#include <stdio.h>

enum open_status  {
    OPEN_OK = 0,
    OPEN_ERROR
};
enum open_status open_f(FILE** file, const char *name, const char* mode);

enum close_status  {
    CLOSE_OK = 0,
    CLOSE_ERROR
};
enum close_status close_f(FILE** file);

#endif
