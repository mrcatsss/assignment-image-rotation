#include "bmp_create.h"
#include "serializer.h"
#include <malloc.h>
#include <stdio.h>


enum read_status from_bmp(FILE* input, struct image* img) {
    struct bmp_header bmp;
    fread(&bmp, sizeof(struct bmp_header), 1, input);
    img -> width = bmp.biWidth;
    img -> height = bmp.biHeight;
    img -> data = malloc(bmp.biSizeImage);
    uint16_t padd = img->width%4;
    for (size_t i = 0; i < bmp.biHeight; i++) {
        fread(&(img -> data[i * img -> width]), sizeof(struct pix), img->width, input);
        fseek(input, padd, SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* output, struct image const* img) {
    struct bmp_header bmp = create_header(img);
    if (fwrite(&bmp, sizeof(struct bmp_header), 1, output) < 1) return WRITE_ERROR;
    uint16_t padd = img->width%4;
    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(&(img->data)[i * img->width], sizeof(struct pix), img->width, output) < img->width) return WRITE_ERROR;
        if(fwrite((struct pix[]) {{0}, {0}, {0}}, 1, padd, output) < padd) return WRITE_PADD_ERROR;
    }
    return WRITE_OK;
}
