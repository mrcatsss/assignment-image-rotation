#include "bmp_create.h"


struct bmp_header create_header(const struct image* image) {
    struct bmp_header header = {
            .bfType = 0x4D42,
            .bfileSize = (image->width) * (image->height) * sizeof(struct pix) + (image->height) * (image->width % 4) + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image -> width,
            .biHeight = image -> height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0};
    header.biSizeImage = header.bfileSize - header.bOffBits;
    return header;
}
