#include "internal.h"
#include "transformer.h"

#include <malloc.h>

size_t get_pix_coords(size_t row, size_t col, size_t img_width) {
    return col+row*img_width;
}

struct image transform(struct image source) {
    uint64_t height = source.height;
    uint64_t width = source.width;
    struct image result = {.height = width, .width = height};
    //uint64_t padd = (4 - width * 3 % 4) % 4;
    //uint64_t padd_r = (4 - height * 3 % 4) % 4;

    result.data = malloc(width * height * sizeof(struct pix));

    for (size_t i = 0; i < height; i = i + 1) {
        for (size_t j = 0; j < width; j = j + 1) {
            // r
            result.data[get_pix_coords(j, height-i-1, height)] = source.data[get_pix_coords(i, j, width)];
        }
    }
    return result;
}

