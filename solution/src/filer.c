#include "internal.h"
#include "opener.h"
#include "serializer.h"
#include <stdio.h>

enum open_status open_f(FILE** file, const char* name, const char* mode) {
    *file = fopen(name, mode);
    if (*file) return OPEN_OK;
    else return OPEN_ERROR;
}

enum close_status close_f(FILE** file) {
    if (fclose(*file) == 0) return CLOSE_OK;
    else return CLOSE_ERROR;
}


