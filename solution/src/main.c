#include "opener.h"
#include "internal.h"
#include "serializer.h"
#include "transformer.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    FILE* input;
    FILE* output;
    struct image source;

    if (argc > 3) return 1;
    char *path_in = argv[1];
    char *path_out = argv[2];

    open_f(&input, path_in, "rb");
    from_bmp(input, &source);
    close_f(&input);
    struct image result = transform(source);

    open_f(&output, path_out, "wb");
    to_bmp(output, &result);
    close_f(&output);
    free(source.data);
    free(result.data);
    return 0;
}

